package hieutm.dev.mockitofundamentals.example8;

import java.util.List;

public class UserMess {

    private final String mFullName;
    private final Address mAddress;
    private final PhoneNumber mPhoneNumber;

    public UserMess(String fullName, Address address, PhoneNumber phoneNumber) {
        mFullName = fullName;
        mAddress = address;
        mPhoneNumber = phoneNumber;
    }

    public void logOut() {
    }

    public void connectWith(UserMess otherUser) {
    }

    public List<UserMess> getConnectedUsers() {
        return null;
    }

    public void disconnectFromAll() {
    }

    public String getFullName() {
        return mFullName;
    }

    public Address getAddress() {
        return mAddress;
    }

    public PhoneNumber getPhoneNumber() {
        return mPhoneNumber;
    }
}
