package hieutm.dev.mockitofundamentals.example8;

import java.util.List;

public class UserObject {

    public void logOut() {
    }

    public void connectWith(UserObject otherUser) {
    }

    public List<UserObject> getConnectedUsers() {
        return null;
    }

    public void disconnectFromAll() {
    }

}
