package hieutm.dev.mockitofundamentals.example7.eventbus;

public interface EventBusPoster {

    void postEvent(Object event);

}
