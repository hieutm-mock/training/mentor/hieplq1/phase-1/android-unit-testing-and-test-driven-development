package hieutm.dev.unittestingfundamentals.example1;

public class PositiveNumberValidator {

    public boolean isPositive(int number) {
        return number > 0;
    }

}
