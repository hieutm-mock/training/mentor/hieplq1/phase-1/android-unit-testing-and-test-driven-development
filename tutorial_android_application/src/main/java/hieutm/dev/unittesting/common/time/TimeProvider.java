package hieutm.dev.unittesting.common.time;

public class TimeProvider {

    public long getCurrentTimestamp() {
        return System.currentTimeMillis();
    }
}
