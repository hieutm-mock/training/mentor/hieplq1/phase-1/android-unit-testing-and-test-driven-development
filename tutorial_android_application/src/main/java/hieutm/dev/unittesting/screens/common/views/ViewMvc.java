package hieutm.dev.unittesting.screens.common.views;

import android.view.View;

public interface ViewMvc {
    View getRootView();
}
