package hieutm.dev.unittesting.screens.common.navdrawer;

public interface NavDrawerHelper {

    void openDrawer();
    void closeDrawer();
    boolean isDrawerOpen();
}
