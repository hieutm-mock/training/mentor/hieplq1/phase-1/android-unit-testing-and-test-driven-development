package hieutm.dev.unittesting.screens.questionslist.questionslistitem;

import hieutm.dev.unittesting.questions.Question;
import hieutm.dev.unittesting.screens.common.views.ObservableViewMvc;

public interface QuestionsListItemViewMvc extends ObservableViewMvc<QuestionsListItemViewMvc.Listener> {

    public interface Listener {
        void onQuestionClicked(Question question);
    }

    void bindQuestion(Question question);
}
