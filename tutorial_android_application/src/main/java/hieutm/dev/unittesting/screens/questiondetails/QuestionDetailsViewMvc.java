package hieutm.dev.unittesting.screens.questiondetails;

import hieutm.dev.unittesting.questions.QuestionDetails;
import hieutm.dev.unittesting.screens.common.views.ObservableViewMvc;

public interface QuestionDetailsViewMvc extends ObservableViewMvc<QuestionDetailsViewMvc.Listener> {

    public interface Listener {
        void onNavigateUpClicked();
    }

    void bindQuestion(QuestionDetails question);

    void showProgressIndication();

    void hideProgressIndication();
}
