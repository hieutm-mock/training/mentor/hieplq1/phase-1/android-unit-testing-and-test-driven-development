package hieutm.dev.unittesting.screens.common.fragmentframehelper;

import android.widget.FrameLayout;

public interface FragmentFrameWrapper {

    FrameLayout getFragmentFrame();
}
