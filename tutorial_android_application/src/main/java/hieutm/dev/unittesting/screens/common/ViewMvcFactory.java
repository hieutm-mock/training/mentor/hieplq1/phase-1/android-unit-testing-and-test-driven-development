package hieutm.dev.unittesting.screens.common;

import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import hieutm.dev.unittesting.screens.common.navdrawer.NavDrawerHelper;
import hieutm.dev.unittesting.screens.common.navdrawer.NavDrawerViewMvc;
import hieutm.dev.unittesting.screens.common.navdrawer.NavDrawerViewMvcImpl;
import hieutm.dev.unittesting.screens.common.toolbar.ToolbarViewMvc;
import hieutm.dev.unittesting.screens.questiondetails.QuestionDetailsViewMvc;
import hieutm.dev.unittesting.screens.questiondetails.QuestionDetailsViewMvcImpl;
import hieutm.dev.unittesting.screens.questionslist.QuestionsListViewMvc;
import hieutm.dev.unittesting.screens.questionslist.QuestionsListViewMvcImpl;
import hieutm.dev.unittesting.screens.questionslist.questionslistitem.QuestionsListItemViewMvc;
import hieutm.dev.unittesting.screens.questionslist.questionslistitem.QuestionsListItemViewMvcImpl;

public class ViewMvcFactory {

    private final LayoutInflater mLayoutInflater;
    private final NavDrawerHelper mNavDrawerHelper;

    public ViewMvcFactory(LayoutInflater layoutInflater, NavDrawerHelper navDrawerHelper) {
        mLayoutInflater = layoutInflater;
        mNavDrawerHelper = navDrawerHelper;
    }

    public QuestionsListViewMvc getQuestionsListViewMvc(@Nullable ViewGroup parent) {
        return new QuestionsListViewMvcImpl(mLayoutInflater, parent, mNavDrawerHelper, this);
    }

    public QuestionsListItemViewMvc getQuestionsListItemViewMvc(@Nullable ViewGroup parent) {
        return new QuestionsListItemViewMvcImpl(mLayoutInflater, parent);
    }

    public QuestionDetailsViewMvc getQuestionDetailsViewMvc(@Nullable ViewGroup parent) {
        return new QuestionDetailsViewMvcImpl(mLayoutInflater, parent, this);
    }

    public ToolbarViewMvc getToolbarViewMvc(@Nullable ViewGroup parent) {
        return new ToolbarViewMvc(mLayoutInflater, parent);
    }

    public NavDrawerViewMvc getNavDrawerViewMvc(@Nullable ViewGroup parent) {
        return new NavDrawerViewMvcImpl(mLayoutInflater, parent);
    }
}
